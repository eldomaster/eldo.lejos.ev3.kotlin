import lejos.hardware.ev3.LocalEV3
import lejos.hardware.sensor.EV3ColorSensor
import lejos.robotics.SampleProvider

/**
 * colorFetcher
 * managing color sensor functions
 * outsourcing code from main class
 * for smoother compiling and better readability
 */
internal class colorFetcher(port: String) {

    private val colProv: SampleProvider
    private val colSample: FloatArray

    init {
        val colSensor = EV3ColorSensor(LocalEV3.get().getPort(port))
        colSensor.setFloodlight(true)
        colProv = colSensor.rgbMode
        colSample = FloatArray(colProv.sampleSize())
    }

    val r: Float
        get() {
            colProv.fetchSample(colSample, 0)
            return colSample[0]
        }

    val g: Float
        get() {
            colProv.fetchSample(colSample, 0)
            return colSample[1]
        }

    val b: Float
        get() {
            colProv.fetchSample(colSample, 0)
            return colSample[2]
        }
}
