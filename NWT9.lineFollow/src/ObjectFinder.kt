import lejos.hardware.ev3.LocalEV3
import lejos.hardware.sensor.EV3UltrasonicSensor
import lejos.robotics.SampleProvider
import lejos.robotics.filter.MeanFilter

class ObjectFinder(port: String) {
    private val sensorUS: EV3UltrasonicSensor = EV3UltrasonicSensor(LocalEV3.get().getPort(port))
    private val usProv: SampleProvider = sensorUS.distanceMode
    // erstellt durchschnitt der letzten 5 Werte
    private val usProvMean: MeanFilter = MeanFilter(usProv, 5)
    private val usSample: FloatArray = FloatArray(usProvMean.sampleSize())


    // gibt den Abstand zu einem Objekt an
    val distance: Float
        get() {
            usProvMean.fetchSample(usSample, 0)
            return usSample[0]
        }

    fun objThere(distanceM: Float): Boolean {
        usProvMean.fetchSample(usSample, 0)
        if(usSample[0] < distanceM) return true
        return false
    }
}