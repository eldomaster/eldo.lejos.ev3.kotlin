import lejos.hardware.Button.*
import lejos.hardware.lcd.LCD
import lejos.hardware.motor.EV3LargeRegulatedMotor
import lejos.robotics.RegulatedMotor

import lejos.hardware.port.MotorPort.*
import lejos.utility.Delay


/**
 * der Farbsensor sitzt vor der Antriebs- bzw Lenkachse!!
 * SPEED = Grundgeschwindigkeit
 * SENSIBILITY = wie stark der Roboter auf veränderugn reagiert ca. 600-1000
 * TSENSIBILITY = starke der reaktion auf lange weiß bzw. schwarz-werte ca. 1-3
 * TMAXRATE = maximale extre kurven drehrate
 * BV = black value des sensors
 * WV = white value des Sensors
 * in float angeben!
 */
const private val SPEED = 360f
const private val SENSIBILITY = 800f
const private val TSENSIBILITY = 2.4f
const private val TMAXRATE = 130f
const private val BV = 0.035f
const private val WV = 0.243f
const private val DISTANCE = 22

private var isBlack: Float = 0f
private var leftMotor: RegulatedMotor = EV3LargeRegulatedMotor(A)
private var rightMotor: RegulatedMotor = EV3LargeRegulatedMotor(B)
private var leftDistance: Int = 0
private var rightDistance: Int = 0
private var hinder: Int = 0

private val fetch = colorFetcher("S2")
private val objFind = ObjectFinder("S1")

fun main(args: Array<String>) {
    LCD.clear()
    LCD.drawString("START?", 0, 5)
    waitForAnyPress()
    LCD.clear()
    LCD.refresh()

    // repeat till ESCAPE-button is pressed
    while ((rightDistance - leftDistance) < DISTANCE && ENTER.isUp) {
        displayData()
        distanceCount()
        if (objFind.objThere(0.20f)) {
            driveAround(290)
        } else {
            followLine()
        }
    }
    leftMotor.stop()
    rightMotor.stop()
    waitForAnyPress()
}

private fun driveAround(degrees: Int) {
    //set motorSpeed
    hinder++
    setMotorSpeed(SPEED, SPEED)
    leftMotor.stop(true)
    rightMotor.stop(true)
    mFlt()
    leftMotor.setAcceleration(2000)
    rightMotor.setAcceleration(2000)
//links
    rightMotor.rotate(270, true)
    leftMotor.rotate(-270)
    mFlt()
//vor
    leftMotor.rotate(400, true)
    rightMotor.rotate(400)
    mFlt()
//rechts
    leftMotor.rotate(degrees, true)
    rightMotor.rotate(-degrees)
    mFlt()
//gerade
    leftMotor.rotate(800, true)
    rightMotor.rotate(800)
    mFlt()
//rechts
    leftMotor.rotate(degrees, true)
    rightMotor.rotate(-degrees)
    mFlt()
//zuruck
    leftMotor.rotate(380, true)
    rightMotor.rotate(380)
    mFlt()
//auf linie zurück (links)
    rightMotor.rotate(275, true)
    leftMotor.rotate(-275)
    mFlt()
    leftMotor.resetTachoCount()
    rightMotor.resetTachoCount()
    isBlack = 0f
}

private fun mFlt() {
    leftMotor.stop(true)
    rightMotor.stop(true)
    Delay.msDelay(250)
}

private fun followLine() {
    setMotorSpeed(SPEED, SPEED)
    leftMotor.forward()
    rightMotor.forward()

    // is getR() "more" black ?
    // regulating the turn
    if (fetch.r < (BV + WV) / 2) {
        if (isBlack < TMAXRATE) isBlack += TSENSIBILITY
    } else {
        if (isBlack > 0) isBlack -= TSENSIBILITY
    }
    setMotorSpeed(leftSpeed, rightSpeed)
}

private fun setMotorSpeed(leftMotorSpeed: Float, rightMotorSpeed: Float) {
    leftMotor.speed = leftMotorSpeed.toInt()
    rightMotor.speed = rightMotorSpeed.toInt()
}

private fun distanceCount() {
    if (leftMotor.tachoCount > 180) {
        leftDistance++
        leftMotor.resetTachoCount()
    }
    if (rightMotor.tachoCount > 180) {
        rightDistance++
        rightMotor.resetTachoCount()
    }
}

private val leftSpeed: Float
    get() = SENSIBILITY * fetch.r + SPEED - isBlack


private val rightSpeed: Float
    get() = SENSIBILITY * (BV + WV) - SENSIBILITY * fetch.r + SPEED + isBlack

private fun displayData() {
    LCD.drawString("Abstand: ${objFind.distance}", 0, 0)
    LCD.drawString("sLinks: ${leftDistance}", 0, 1)
    LCD.drawString("sRechts: ${rightDistance}", 0, 2)
    LCD.drawString("sDif: ${rightDistance - leftDistance}", 0, 3)
    LCD.drawString("Hindernisse: ${hinder}", 0, 4)

    LCD.drawString("R: ${fetch.r}", 0, 6)
    LCD.refresh()
}